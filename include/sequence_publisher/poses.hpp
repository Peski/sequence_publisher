
#ifndef POSES_HPP_
#define POSES_HPP_

// STL
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <istream>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace poses {

struct pose_t {
    double tx, ty, tz;
    double qw, qx, qy, qz;

    pose_t()
        : tx(0.0), ty(0.0), tz(0.0), qw(1.0), qx(0.0), qy(0.0), qz(0.0)
    { }

    pose_t(double tx, double ty, double tz, double qw, double qx, double qy, double qz)
        : tx(tx), ty(ty), tz(tz), qw(qw), qx(qx), qy(qy), qz(qz)
    {
        q_normalize();
    }

    inline void q_normalize() {
        double norm = std::sqrt(qw*qw + qx*qx + qy*qy + qz*qz);
        qw /= norm;
        qx /= norm;
        qy /= norm;
        qz /= norm;
    }
};

std::unordered_map<std::uint64_t, pose_t> read_poses_file(const std::string &path, char delim = ',') {
    std::unordered_map<std::uint64_t, pose_t> timestamp2pose;

    std::string line;
    std::ifstream input(path);
    while (std::getline(input, line)) {
        if (line.empty()) continue;
        if (line.front() == '#') continue;

        std::uint64_t timestamp;
        std::string str_ts, str_tx, str_ty, str_tz, str_qw, str_qx, str_qy, str_qz;
        std::stringstream line_stream(line);
        if (std::getline(line_stream, str_ts, delim)
              && std::getline(line_stream, str_tx, delim)
              && std::getline(line_stream, str_ty, delim)
              && std::getline(line_stream, str_tz, delim)
              && std::getline(line_stream, str_qw, delim)
              && std::getline(line_stream, str_qx, delim)
              && std::getline(line_stream, str_qy, delim)
              && std::getline(line_stream, str_qz, delim)) {
            timestamp = static_cast<std::uint64_t>(std::stoll(str_ts));
            timestamp2pose[timestamp] = pose_t(std::stod(str_tx),
                                               std::stod(str_ty),
                                               std::stod(str_tz),
                                               std::stod(str_qw),
                                               std::stod(str_qx),
                                               std::stod(str_qy),
                                               std::stod(str_qz));
        }
    }

    return timestamp2pose;
}

std::unordered_map<std::uint64_t, std::string> read_images_file(const std::string &path, char delim = ',') {
    std::unordered_map<std::uint64_t, std::string> timestamp2file;

    std::string line;
    std::ifstream input(path);
    while (std::getline(input, line)) {
        if (line.empty()) continue;
        if (line.front() == '#') continue;

        std::uint64_t timestamp;
        std::string value, file;
        std::stringstream line_stream(line);
        if (std::getline(line_stream, value, delim) && std::getline(line_stream, file, delim)) {
            timestamp = static_cast<std::uint64_t>(std::stoll(value));
            timestamp2file[timestamp] = file;
        }
    }

    return timestamp2file;
}

} // namespace poses

#endif // POSES_HPP_
