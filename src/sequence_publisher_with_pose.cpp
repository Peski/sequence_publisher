
#define PROGRAM_NAME \
    "sequence_publisher_with_pose"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, rate, 100.0, "Maximum loop rate (Hz)")

#define ARGS_CASES                                                                                 \
    ARG_CASE(images_directory)                                                                     \
    ARG_CASE(images_file)                                                                          \
    ARG_CASE(poses_file)                                                                           \
    ARG_CASE(images_topic)                                                                         \
    ARG_CASE(trajectory_topic)

#include <signal.h>

#include <atomic>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include <ros/ros.h>
#include <image_transport/image_transport.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseStamped.h>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <Eigen/Core>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sequence_publisher/util/macros.h>
#include <sequence_publisher/util/version.h>

#include <sequence_publisher/args.hpp>
#include <sequence_publisher/poses.hpp>
#include <sequence_publisher/sequence.hpp>

enum class MetaData {
  NO_DATA,       // = 0
  IMAGE,         // = 1
  POSE,          // = 2
  IMAGE_AND_POSE // = 3
};

namespace fs = boost::filesystem;

std::atomic<bool> exit_requested;

void signal_handler(int sig) {
  exit_requested = true;
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_rate > 0.0);
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_directory(ARGS_images_directory));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_images_file));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_poses_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    ros::init(argc, argv, PROGRAM_NAME, ros::init_options::NoSigintHandler);
    ros::NodeHandle nh;

    exit_requested = false;
    signal(SIGINT, signal_handler);

    // topic (images)
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise(ARGS_images_topic, 30);

    // topic (poses)
    ros::Publisher pub_ = nh.advertise<geometry_msgs::PoseStamped>(ARGS_trajectory_topic, 1000);

    std::cout << "Loading..." << '\r' << std::flush;

    // timestamps
    std::unordered_map<std::uint64_t, std::string> ts2file;
    ts2file = poses::read_images_file(ARGS_images_file);

    // base path
    fs::path base_path(ARGS_images_directory);

    // trajectory
    std::unordered_map<std::uint64_t, poses::pose_t> ts2pose = poses::read_poses_file(ARGS_poses_file);

    std::map<std::uint64_t, MetaData> metadata;
    // images
    for (const std::pair<std::uint64_t, std::string>& entry : ts2file) {
        metadata[entry.first] = MetaData::IMAGE;
    }
    // poses
    for (const std::pair<std::uint64_t, poses::pose_t>& entry : ts2pose) {
        if (metadata.find(entry.first) != metadata.end()) {
            metadata[entry.first] = MetaData::IMAGE_AND_POSE;
        } else {
            metadata[entry.first] = MetaData::POSE;
        }
    }

    // TODO More accurate rate between loops
    //      (measuring loop execution time and fetching next timestamp to compute sleep time)

    size_t i = 0, n = ts2file.size();
    auto itr = metadata.begin();
    ros::Rate loop_rate(FLAGS_rate);
    while (!exit_requested && nh.ok() && itr != metadata.end()) {
        std::cout << i+1 << " / " << n << '\r' << std::flush;

        std::uint64_t ts = itr->first;
        MetaData md = itr->second;

        if (md == MetaData::IMAGE || md == MetaData::IMAGE_AND_POSE) {
          // image
          std::string image_name = ts2file[ts];
          ++i;

          // header
          std_msgs::Header header;
          header.stamp = ros::Time(static_cast<double>(ts) * 1e-9);

          cv::Mat image = cv::imread((base_path / image_name).string(), CV_LOAD_IMAGE_COLOR);
          sensor_msgs::ImagePtr msg = cv_bridge::CvImage(header, "bgr8", image).toImageMsg();

          pub.publish(msg);
        }

        if (md == MetaData::POSE || md == MetaData::IMAGE_AND_POSE) {
            // pose
            const poses::pose_t& p = ts2pose[ts];

            geometry_msgs::PoseStamped msg_;
            msg_.header.stamp = ros::Time(static_cast<double>(ts) * 1e-9);
            msg_.pose.position.x = p.tx;
            msg_.pose.position.y = p.ty;
            msg_.pose.position.z = p.tz;
            msg_.pose.orientation.x = p.qx;
            msg_.pose.orientation.y = p.qy;
            msg_.pose.orientation.z = p.qz;
            msg_.pose.orientation.w = p.qw;

            pub_.publish(msg_);
        }

        ++itr;

        ros::spinOnce();
        loop_rate.sleep();
    }
    std::cout << std::endl;

    ros::shutdown();
    return 0;
}
